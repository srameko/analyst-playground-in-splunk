# Analyst Playground in Splunk

Playground in Splunk based on BOTSv1, BOTSv2 data set (opensourced by Splunk)

## BOTSv1

Data set was opensourced by [Splunk](https://github.com/splunk/botsv1). It covers two Scenarios - APT and Ransomware.

As source we can use logs from Suricata, Fortigate UTM, network (DNS, SMTP, HTTP, SMB,...) and Windows (Sysmon, IIS, Registry, EventLog:App, EventLog:Security, EventLog:System)

In this image we will work with attack only data. It is also possible work with all data, but you need more free space on your workstation (like +120GB)

## BOTSv2

Data set was opensourced by [Splunk](https://github.com/splunk/botsv2). It covers five Scenarios - APT, endpoint security, web application security, fraud and insider threat.

## Splunk Vagrant Image

### Prerequisities

* Lastest [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
* Lastest [Vagrant](https://www.vagrantup.com/downloads.html)
* Lastest [git](https://git-scm.com/downloads)

### Get this Repo

Navigate into directory with your git projects and run `git clone git@gitlab.com:srameko/analyst-playground-in-splunk.git`

### Setup Splunk Image with BOTSv1/BOTSv2

To spin up this image just navigate into downloaded folder and type

  ```
  $ vagrant --bots-version=v1 up
  ```

For BOTS v1, for BOTS v2 just change `v1` to `v2`

After a while it should show you address and credentials to use for login

## Analysis

Analysts notes what to look for are avaliable on [my](https://docs.ondrejsramek.cz/notes/security/splunk-bots) website.
